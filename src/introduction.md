
> Please note that this site is under construction and content is being added!

# Introduction
My name is Henrik Melsom and I’m a technical artists, passionate about all things graphics!

I’ve studied [Computer Graphics at Luleå University of Technology](https://www.ltu.se/edu/program/KKDGG/KKDGG-Datorgrafik-konstnarlig-kandidat-1.76764?l=en) and is currently working as Technical Artist at [Tar Valley](https://tarvalley.com/).

Checkout my [work](./work.md), [resumé](./resume.md) and [contact info](./contact.md)!

## Eye Catchers
Here's some pictures I contributed on, hope you stay for a while!

![Kontroll Screenshot](img/introduction/screen_01.png)
I did the lighting, material/shader work and level/environment design. [Check it out!](./work/kontroll.md)

![Principia Screenshot](img/introduction/screen_02.png)
I did the lighting and some material/shader work. [Check it out!](./work/principia.md)

![The Cistern Screenshot](img/introduction/screen_03.png)
I did everything except character modeling. [Check it out!](./work/the-cistern.md)