# Contact
If you want to get in touch you can reach me via the channels below!

<table style="margin: 0 0">
  <tr>
    <td>Mail</td>
    <td><a href="mailto:melsom.henrik@gmail.com">melsom.henrik@gmail.com</a></td>
  </tr>
  <tr>
    <td>LinkedIn</td>
    <td><a href="https://www.linkedin.com/in/henrik-melsom">https://www.linkedin.com/in/henrik-melsom</a></td>
  </tr>
</table>