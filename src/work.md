# Work
This is unfortunatly not complete, but I'm currently adding more content both old and new!

### [The Cistern](./work/the-cistern.md)
This was a school project for the Realtime Graphics course at Luleå Unversity of Technology.

### [Principia](./work/principia.md)
This was a school project for the Game Production course at Luleå University of Technology.

### [Kontroll](./work/kontroll.md)
*“Sidescroller problem solving adventure on a mission to save those whom you love.”*  

### [Shader Animated Logo](./work/shader-animated-logo.md)
I had some spare time one evening and decided to do some animated logo design with shaders.

### [Procedural Lightning](./work/procedural-lightning.md)
I made some procedural lightning in Houdini for *secret project*.
