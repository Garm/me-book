# Resumé 
## Experience 
### Winner, Arctic Game Pitch, 2019
Pitch contest held by Arctic Game Lab, 5 winners (of 23 this year) are selected to go to Gamescom.  
Me and my group (5 people total) battled our way to a Gamescom Trade Visitor Ticket.
### Developer, NFS Heat Studio, 2019
Worked as a contractor on the *Need for Speed Heat* companion app.  
Main responsibilties was porting Frostbite car shaders to Unity and tools development.  
Wrote a complete custom lighting model and shader system with inspiration from Unity's LWRP.
### Technical Artist, Tarvalley, 2018-now
Started as 3D Generalist intern and quickly distinguished as Technical Artist.  
Main responsibility was solving the technical challenges, mostly 3D graphics related.  
Scope also include film VFX, modeling, texturing, tool development, web
development, shader development and more. 

## Education
<table style="margin: 0 0">
  <tr>
    <th>Program</th>
    <th>School</th>
    <th>End Year</th>
  </tr>
  <tr>
    <td>Computer Graphics</td>
    <td>LTU</td>
    <td>2018</td>
  </tr>
  <tr>
    <td>Teknik</td>
    <td>Västermalm</td>
    <td>2014</td>
  </tr>
</table>

## Softwares 
### DCC
<table style="margin: 0 0">
  <tr>
    <th>Software</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Houdini</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Substance Designer</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Substance Automation Kit</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>Substance Painter</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>Blender</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Maya</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>3DS Max</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>Photoshop</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Illustrator</td>
    <td>Excellent</td>
  </tr>
</table>

### Game Engines
<table style="margin: 0 0">
  <tr>
    <th>Software</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Unreal Engine 4</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Unreal Development Kit</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>Unity3D</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Amethyst</td>
    <td>Capable</td>
  </tr>
  <tr>
    <td>Haxe</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>XNA</td>
    <td>Good</td>
  </tr>
</table>

### Version Control
<table style="margin: 0 0">
  <tr>
    <th>Software</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Git</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>SVN</td>
    <td>Good</td>
  </tr>
</table>

### Other
<table style="margin: 0 0">
  <tr>
    <th>Software</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Node.js</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>openFramework</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>p5.js</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>ShaderToy</td>
    <td>Excellent</td>
  </tr>
</table>
and more... 

## Programming Languages
<table style="margin: 0 0">
  <tr>
    <th>Language</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Python</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>Javascript/Typescript</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>C#</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>C++</td>
    <td>Capable</td>
  </tr>
  <tr>
    <td>Java</td>
    <td>Capable</td>
  </tr>
  <tr>
    <td>Go</td>
    <td>Capable</td>
  </tr>
  <tr>
    <td>Rust</td>
    <td>Good</td>
  </tr>
  <tr>
    <td>GLSL</td>
    <td>Excellent</td>
  </tr>
  <tr>
    <td>HLSL</td>
    <td>Excellent</td>
  </tr>
</table>
and more... 

## Real Life Languages
<table style="margin: 0 0">
  <tr>
    <th>Language</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Swedish</td>
    <td>Native</td>
  </tr>
  <tr>
    <td>English</td>
    <td>Excellent</td>
  </tr>
</table>

## Personal Interests 
- Playing Games 
- Making Games 
- Creative Coding 
- Electronics 
- Shaders 
- Game Design 
- UX 
- The environment 
- and more...
