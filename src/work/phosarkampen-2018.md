# Phösarkampen 2018

This was a CCG project with visually unique cards for the yearly welcome of new students at Campus Skellefteå.

All the card graphics except for the portraits were procedurally generated in Substance Designer. 

![Example output](/img/work/phosarkampen-2018/example_01.png)

To batch out a complete deck and provide automatic inputs like name, portrait, cardtype, etc. Substance Automation Toolkit was used to pipe card data into Substance Designer and generate all the cards. 

> This page is WIP! There's lots more to talk about this.