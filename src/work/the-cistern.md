# The Cistern
This was a school project for the Realtime Graphics course at Luleå Unversity of Technology.

The project lasted 2.5 weeks and character highpoly mesh and animations were provided when starting. The goal was to create a playable topdown puzzle level with, in my case Unity.

Everything was done by me excluding the following:
- Character high poly mesh
- Foliage (ferns)
- Spike and crushing traps

## Video
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/209196207?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<a href="https://vimeo.com/209196207">Watch at Vimeo.</a>

## Fun facts
### My first vertex paint shader in Unity
![Vertex paint example](../img/work/the-cistern/vertex-paint.png)

### Procedural root generation
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/336435063?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<a href="https://vimeo.com/336435063">Watch at Vimeo.</a>

### Tiled Map integration
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/336437168?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<a href="https://vimeo.com/336437168">Watch at Vimeo.</a>