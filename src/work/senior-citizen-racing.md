# Senior Citizen Racing

Submission for the Epic MegaJam 2020 with a team of 5 ([itch.io page](https://arcticbeardstudio.itch.io/senior-citizen-racing) (I know the page is very minimal at best)).

This was a really fun project because I got use Houdini for almost all of my work on this.
The car physics were done from scratch by me which I think is pretty close to Mario Kart considering the time spent.

> This page is WIP! There's lots more to talk about this.

## Summary
- [Grass](#grass) - Simple normal trick and shader work. 
- [Scooter Parts](#scooter-parts) - Batch model preprocessing in Houdini.

## Grass

I just have to mention the grass, which I'm particularly happy with. It's quite simple really, a flat piece of geometry with an alpha cutout texture and normals pointing up, pretty trivial but effective (there's of course more going on in the shader like wind and such but nothing too fancy)!

![Grass in Blender](/img/work/senior-citizen-racing/grass-blender.png)  
Snip of grass in Blender.

<iframe src="https://player.vimeo.com/video/491373947" width="540" height="304" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe><br>
In-game view.

## Scooter Parts

There are 19 different parts you can customize your scooter from (it's actually 30 if you include parts added based on some of the customizable parts).

They were all modeled by another artist on the team in Maya. 