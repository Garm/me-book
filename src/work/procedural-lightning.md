# Procedural Lightning
I made some procedural lightning in Houdini for *secret project*. There are two variants which both outputs a mesh for fast rendering!

## Line Subdivision
![Lightning bolt examples](/img/work/procedural-lightning/subdiv-line-example.png)

The first method is a simple but effective technique, it's basically consists of two similar operations.

> **Jitter**  
> Subdivides every line and offsets the new point based on the length of the original line.

> **Fork**  
> Same as above but also creates a new line from the new point extending in the direction of the offset.

This is implemented as a simple to use node in Houdini wich *jitters* polylines with an option to also *fork* it.  
It was super easy and fun to model different lighting bolt structures with this!

Also included are some utility nodes to create a starting point and also meshing the resulting lightning bolt.

Check out this awesome [set of slides from Nvidia](http://developer.download.nvidia.com/SDK/10/direct3d/Source/Lightning/doc/lightning_doc.pdf) for more details (and great rendering method)!

## Rapidly-exploring Random Tree
![Lightning spread examples](/img/work/procedural-lightning/rrt-example.png)

This is more complex and slower to generate but still usable and a cool effect.

It's similar to space colonization algorithms but instead of branches moving towards the nearest seed points they are picked at random which creates a more jagged look.

> I call them seed points but you can also think of them as target points.

This was also implemented as a node in Houdini taking in two point clouds one containing source points and the other seed points.   
And then it just runs the algorithm until all seeds are reached or the max iteration cap is reached.

You can also target a specific point and run it until that point is reached for a more directed approach.

This [answer on StackExchange](https://gamedev.stackexchange.com/a/100583) was a great help!