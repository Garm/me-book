# Principia
This was a school project for the Game Production course at Luleå University of Technology.

The course goal was making a first person puzzle game utilizing some form of AI in 8 weeks. No violence was allowed.

So my group decided to create a atmospheric game where the puzzle mechanics revolved around weightlessness and a physics gun.  
2 weeks of planning and 6 weeks of production later and we had a game, it has an unfinished look becuae we had an ambitious goal but it's not too bad considering the time frame.  
It had potential but the physics gun mechanic ended up being really clunky and some puzzles were too unintuative.  
So what's the lesson? QA and realistic goal/time estimation is important!

## Video
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://www.youtube.com/embed/IQZr1nzC2_k" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
<a href="https://www.youtube.com/watch/IQZr1nzC2_k">Watch at YouTube.</a>

## Fun Facts
### Procedural Environment Variation
I applied som very basic procedural generation techniques to break up our modular meshes.  

Some materials (like floor, wall and ceiling) has some random generation in them to make them pick a random texture from an atlas it's very crude but it looks better.

Also the corridors have two walls to pick from which are randomly selected when placed. It's not much, but it really made a difference!

Unfortunatly out time plan didn't allow for more sofisticated techniques and more content, i think it could have looked much better with more love, both the handcrafted and generated content.

### Automatic Texturing
By assigning color coding our material assignments, we could automatically applied prebuilt Substance Designer materials in Substance Painter.  
This made our texture authoring a bit faster, at the cost of creating the material library of course.