# Shader Animated Logo
I had some spare time one evening and decided to do some animated logo design with shaders.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://www.shadertoy.com/embed/wdsGDH?gui=true&t=10&paused=true&muted=false" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe></div>
Press play to start or <a href="https://www.shadertoy.com/view/wdsGDH">watch at ShaderToy.</a>

It's simply just drawing with a bunch of SDF functions nothing fancy, the tricky part was animating the water ripples.  

Yes it's both water and beard, you can't see it? ;)  

My first approach was also very aliased but it was hackily solved by raising the SDF function to a high exponent and clamping instead of stepping.