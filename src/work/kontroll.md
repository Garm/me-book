> Keep in mind that everything on this page is still in development and far from finished!

# Kontroll
This is a new game me and some friends are working on!

The best oneliner I got is:  
*“Sidescroller problem solving adventure on a mission to save those whom you love.”*  
Very deep indeed.

Not much more to tell at the moment but I can show some things (not much though).

## Videos
### Pitch Trailer
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/336622045?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<a href="https://vimeo.com/336622045">Watch at Vimeo.</a>

### Weapon Select Prototype
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/330352876?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<a href="https://vimeo.com/330352876">Watch at Vimeo.</a>
